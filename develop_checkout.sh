#!/bin/sh
## @author : saurabh1369@live.com

# imports variables from config file
. ./branches.config

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

cd $projectPath

current_branch=$(git symbolic-ref --short -q HEAD)

if [ "$current_branch" = "$developBranch" ]
then
    echo
    echo "------------------------------------------------------------------------------------------------------------------------"
    echo "You are already on ${GREEN} $developBranch ${NC} branch......"
    echo "------------------------------------------------------------------------------------------------------------------------"
    echo
else
  if [ `git branch --list $developBranch` ]
  then
    echo "------------------------------------------------------------------------------------------------------------------------"
    git checkout $developBranch
    git pull origin $developBranch
  else
    echo "------------------------------------------------------------------------------------------------------------------------"
    git fetch && git checkout $developBranch
  fi
fi