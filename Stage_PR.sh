#!/bin/sh
## @author : saurabh1369@live.com

# imports variables from config file
. ./branches.config

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

cd $projectPath

current_branch=$(git symbolic-ref --short -q HEAD)

git checkout $stageBranch
git pull origin $stageBranch

echo "------------------------------------------------------------------------------------------------------------------------"
echo "Merging $featureBranch branch...."
echo
git pull origin $featureBranch
echo
echo

echo "------------------------------------------------------------------------------------------------------------------------"
echo "Merging $stagePull branch...."
echo
git pull origin $stagePull
echo
echo


CONFLICTS=$(git ls-files -u | wc -l)
if [ "$CONFLICTS" -gt 0 ] ; then
   echo "------------------------------------------------------------------------------------------------------------------------"
   echo "${RED} There is a merge conflict. Aborting ${NC}"
   echo "------------------------------------------------------------------------------------------------------------------------"
   exit 1
else
    echo "-----------------------------------------------------------------------------------------------------------------------"
    echo "Push to $stageBranch branch...."
    git push origin $stageBranch
fi