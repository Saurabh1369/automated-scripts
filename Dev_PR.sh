#!/bin/sh
## @author : saurabh1369@live.com

# imports variables from config file
. ./branches.config

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

cd $projectPath

current_branch=$(git symbolic-ref --short -q HEAD)

if [ "$current_branch" = "$featureBranch" ]
then
    echo
    echo "------------------------------------------------------------------------------------------------------------------------"
    echo "Pushing on ${GREEN} $featureBranch ${NC} branch......"
    git push origin $featureBranch
    echo "------------------------------------------------------------------------------------------------------------------------"
    echo
fi

git checkout $developBranch
git pull origin $developBranch

echo "------------------------------------------------------------------------------------------------------------------------"
echo "Merging feature branch...."
echo
git pull origin $featureBranch
echo
echo

echo "------------------------------------------------------------------------------------------------------------------------"
echo "Merging $developPull branch...."
echo
git pull origin $developPull
echo
echo


CONFLICTS=$(git ls-files -u | wc -l)
if [ "$CONFLICTS" -gt 0 ] ; then
   echo "------------------------------------------------------------------------------------------------------------------------"
   echo "${RED} There is a merge conflict. Aborting ${NC}"
   echo "------------------------------------------------------------------------------------------------------------------------"
   exit 1
else
    echo "-----------------------------------------------------------------------------------------------------------------------"
    echo "Push to develop branch...."
    git push origin $developBranch
fi