#!/bin/sh
## @author : saurabh1369@live.com

# imports variables from config file
. ./branches.config

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

cd $projectPath

current_branch=$(git symbolic-ref --short -q HEAD)

if [ "$current_branch" = "$stageBranch" ]
then
    echo
    echo "------------------------------------------------------------------------------------------------------------------------"
    echo "You are already on ${GREEN} $stageBranch ${NC} branch......"
    echo "------------------------------------------------------------------------------------------------------------------------"
    echo
else
  if [ `git branch --list $stageBranch` ]
  then
    echo "------------------------------------------------------------------------------------------------------------------------"
    git checkout $stageBranch
    git pull origin $stageBranch
  else
    echo "------------------------------------------------------------------------------------------------------------------------"
    git fetch && git checkout $stageBranch
  fi
fi