#!/bin/sh
## @author : saurabh1369@live.com

# imports variables from config file
. ./branches.config

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

cd $projectPath

current_branch=$(git symbolic-ref --short -q HEAD)

if [ "$current_branch" = "$featureBranch" ]
then
    echo
    echo "------------------------------------------------------------------------------------------------------------------------"
    echo "Pushing on ${GREEN} $featureBranch ${NC} branch......"
    git push origin $featureBranch
    echo "------------------------------------------------------------------------------------------------------------------------"
    echo
fi