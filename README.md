# Automated PR creation scripts
The scripts automates the repetitive tasks required for PR creation.
* Works for all projects that follows feature, dev, stage branching.
* Easy switching between branches.
* Reduces human error.


## How to setup
1. Clone this repo
1. Copy feature, dev, stage branch names from your ticket / story.
1. Paste the branch names in branches.config file.
1. Copy full path of your target project directory.
1. Paste project path in branches.config file

example of branches.config file
```
developPull=Develop_Q3_2020
stagePull=Stage_Q2_2021

featureBranch=TUR-54321-form-milestones-database-feature
developBranch=TUR-54321-form-milestones-database-dev
stageBranch=TUR-54321-form-milestones-database-stage

projectPath=/home/saurabh/Desktop/office/thread-electronic-data-capture/
```


## Scripts
- **branches.config** : Represents branch configurations
- **Dev_PR.sh** : Creates PR for development environment
- **Stage_PR.sh** : Creates PR for stage environment
- **develop_checkout.sh** : Switch current branch to develop branch
- **feature_checkout.sh** : Switch current branch to feature branch
- **stage_checkout.sh** : Switch current branch to stage branch
- **push_feature.sh** : Push changes to feature branch

**Note:** 
1. User required to manually add & commit changes.
1. All scripts takes branch names from branches.config file.

---

### Dev_PR.sh
* Switch current branch to `<develop branch>`
* Pulls new changes from remote `<develop branch>`
* Merges remote `<feature branch>`
* Merges Develop_Q3_2020
* Aborts process if conflicts found and shows conflicts error message.
* Push changes to remote `<develop branch>` in case of no conflicts.

### Stage_PR.sh
* Switch current branch to `<stage branch>`
* Pulls new changes from remote `<stage branch>`
* Merges remote `<feature branch>`
* Merges Stage_Q2_2021
* Aborts process if conflicts found and shows conflicts error message.
* Push changes to remote `<stage branch>` in case of no conflicts.

### feature_checkout.sh
* Checkouts to `<feature branch>`, if current branch is not `<feature branch>`
* pull new changes from `<feature branch>`

### push_feature.sh
* If current branch is `<feature branch>`. Push changes to remote `<feature branch>`.

### develop_checkout.sh
* Checkouts to `<develop branch>`, if current branch is not `<develop branch>`
* pull new changes from `<develop branch>`

### stage_checkout.sh
* Checkouts to `<stage branch>`, if current branch is not `<stage branch>`
* pull new changes from `<stage branch>`


